package io.igate.springmongdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringMongDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringMongDemoApplication.class, args);
    }

}
