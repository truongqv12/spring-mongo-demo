package io.igate.springmongdemo.dto.Sector;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.igate.springmongdemo.document.Sector;

import java.util.List;

public class ListSectorDTO {

    private int totalPages;
    private int totalElements;
    private int page;
    private int size;
    private List<Sector> content;

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public int getTotalElements() {
        return totalElements;
    }

    public void setTotalElements(int totalElements) {
        this.totalElements = totalElements;
    }

    public List<Sector> getContent() {
        return content;
    }

    public void setContent(List<Sector> content) {
        this.content = content;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }
}
